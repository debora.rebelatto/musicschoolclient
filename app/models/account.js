import DS from 'ember-data';

var attr = DS.attr;

export default DS.Model.extend({
  login: attr('string'),
  password: attr('string'),
  createdAt: attr('date'),
  updatedAt: attr('date'),
});