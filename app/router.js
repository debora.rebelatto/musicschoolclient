import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('home', { path: '/home' } );
  this.route('login', {path: '/login'});
  this.route('signup', {path: '/signup'});
  this.route('createSubject', { path: '/createsubject' } );
  this.route('createClassroom', { path: '/createclassroom' });
  this.route('listClasses', { path: '/listclasses'} )
});

export default Router;
