import { inject as service } from '@ember/service';
import Route from '@ember/routing/route';

export default Route.extend({
	notifications: service('notification-messages'),

  actions: {
  	submit(name, role, username, email, password, cpassword) {
			let emptyFields = [];
			let count = 0;

			if(!name) { count++; emptyFields.push(' name'); }
			if(!email) { count++; emptyFields.push(' email') }
			if(!username) { count++; emptyFields.push(' username') }
			if(!cpassword) { count++; emptyFields.push(' password confirmation') }

			if(password){
				if(password !== cpassword) {
					count++;
					this.get('notifications').warning('Passwords do not match. Try again!', { autoClear: true });
	 			}

	 			if(password.length < 6) {
	 				count++;
	 				this.get('notifications').warning('Minimum password characters: 6', { autoClear: true, }); 
	 			}
			} else {
				count++; emptyFields.push('password')
			}

			if(count !== 0) {
				this.get('notifications').warning(`Fill out the following fields: ${emptyFields}`, { autoClear: true });
			}
		}
  }
});