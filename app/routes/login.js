import { inject as service } from '@ember/service';
import Route from '@ember/routing/route';

export default Route.extend({
	notifications: service('notification-messages'),

  actions: {
  	submit(email, password) {
  		let emptyFields = [];
			let count = 0;

  		if(!email) { count++;  emptyFields.push(' email') }

  		if(!password){ count++; emptyFields.push(' password') }

  		if(count !== 0) {
				this.get('notifications').warning(`Fill out the following fields: ${emptyFields}`, { autoClear: true });
			}
		}	
  }
});